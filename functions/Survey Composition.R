
cat("audienceComp, ")

audienceComp <- function(finalterms, targs, comptable, extradem = NULL){

    # Build audience comp for demos. audcmpBuild also calls this function
    # Args:
    #   finalterms: LIST  :final.terms from model...
    #       ...keys = 'media', 'demos', 'inter'(interactions)
    #   targs: DATAFRAME :targets tab from guide...
    #       ...columns =
    #       ...variable : CHR :variable name of target
    #       ...target_name : CHR: human readable label of target
    #       ...EScoding : CHR: ES coding logic to create the target
    #   comptable: DATAFRAME : composition data... SURVEY I THINK (?)
    #       ...columns =
    #       ...rpXXX.*.exp: NUM : Average exposures for that democombo
    #       ...<<DEMOGRAPHIC FLAGS>>: int: Boolean Integer (0 or 1); define democombos
    #   extradem: STR (length 1): A variable for a demo that is not in the model
    #       ... POSSIBLY COULD BE OTHER REASONS TO USE extradem?
    #

    # Convert comptable to data.table
    comptable <-
        data.table(comptable)

    # Convert targs to data.table
    # targs <- data.table(targs) # Work through code first befor doing

    # Extract chr vector of audience composition demos
   audcmp.demos <-
       unique(
           unlist(
               c(
                   finalterms$demos,
                   extradem,
                   sapply(
                       strsplit(finalterms$inter, ":"),
                       function(x) {
                          x[[2]]
                           }
                       )
                   )
               )
           )


    audcmpSurvey <-
        melt(
            comptable,
            id.vars = audcmp.demos,
            measure.vars = targs$variable,
            variable.name = 'target')[value == 1, ]

    audcmpSurvey[, value := NULL]

    audcmpSurvey <-
        audcmpSurvey[
            ,
            list('qre.sample' = .N),
            by = c('target', audcmp.demos)]

    return(audcmpSurvey)

}


cat("audcmpBuild, ")

audcmpBuild <- function(dems, targs, comptable, finalterms,
                        es, es.varlists, audcmp, extradem) {

    # Builds audience comp for each demo to be merged with
    # lbd to create heatmap
    #
    #
    # Args:
    #   dems:  DATAFRAME : demos tab from guide
    #       ...columns =
    #       ...variable : CHR :variable name of demo
    #       ...demo_name : CHR: human readable label of demo
    #       ...EScoding : CHR: ES coding logic to create the demo
    #   targs: DATAFRAME :targets tab from guide...
    #       ...columns =
    #       ...variable : CHR :variable name of target
    #       ...target_name : CHR: human readable label of target
    #       ...EScoding : CHR: ES coding logic to create the target
    #   comptable: DATAFRAME : composition data... SURVEY I THINK (?)
    #       ...columns =
    #       ...rpXXX.*.exp: NUM : Average exposures for that democombo
    #       ...<<DEMOGRAPHIC FLAGS>>: int: Boolean Integer (0 or 1); define democombos
    #   finalterms: LIST  :final.terms from model...
    #       ...keys = 'media', 'demos', 'inter'(interactions)
    #   es: DATA.TABLE : es weight values for democombos
    #       ...columns =
    #       ...weight = NUM : weight assigned to that democombo
    #       ...democombo = INT : democombo ID for that row
    #       ...year.flag = INT : year of which the row of data is from (ie 2013)
    #       ...exelate.flag = INT : Boolean Integer (0 or 1); Same for all rows;
    #                               1 if exelate segments included, 0 otherwise.
    #       ...<<DEMOGRAPHIC FLAGS>> = INT: Boolean Integer (0 or 1); define democombos
    #   es.varlists: LIST : Varlists from guide
    #       ...keys =
    #       ...<<VARLIST NAME>>: CHR: names of variables contained in <<VARLIST NAME>>
    #   audcmp: DATATABLE: Provides sample (IS THIS WEIGHT?) and qre.sample for democombos.
    #       ...columns =
    #       ...qre.sample = NUM : NOT SURE (Some Kind of weight?)
    #       ...sample = NUM : NOT SURE (Some Kind of weight?)
    #       ...es.sample = NUM: NOT SURE (Some Kind of weight?)
    #       ...<<DEMGRAPHIC FLAGS>> INT/NUMERIC: Boolean (0 or 1); define democombos

    ## Initialize Lists
    # Extract model demos from final terms.
    # Need to get unique character vector of demographics
    # that have main effects or are in interaction.
    # Assume finalterms$inter has form <<MEDIA VARIABLE>>:<<DEMO VARIABLE>>

    model.demos <-
        unique(
            c(
                finalterms$demos,
                sapply(
                        strsplit(finalterms$inter, ":"),
                        function(x) {
                            x[[2]]
                        }
                )
            )
        )

    # Extract char vector of demos in the demos guide tab that did not
    # Appear in either the main effects or interactions of the final model.
    non.model.demos <-
        setdiff(dems$variable, model.demos)

    audcmp.list <-
        list()
            
    if (length(non.model.demos) > 0) {

        print("Building survey audience comp...")

        # Copy; will test after to lapply to make sure nothing changed.
        ## finalterms.init <- copy(finalterms)
        ## targs.init <- copy(targs)
        ## comptable.init <- copy(comptable)

        # For each demo in non.model.demos (demos that were not in the model):
        # Apply audienceComp function, placing results in a data.frame...
        # ...which has an added column of 'heat.demo' := demo. The colname of
        # ...demo is then set to 'heat.demo.val'
        # The result is a stack of datatables that could be rbindlisted.
        list.survey <-
            lapply(
                non.model.demos,
                function(x) {# x <- "vlist.2"
                    hold <-
                        data.table(
                            data.frame(
                                heat.demo = x,
                                audienceComp(
                                    finalterms = finalterms,
                                    targs = targs,
                                    comptable = comptable,
                                    extradem = unique(c(extradem, x))
                                )
                            )
                        )

                    setnames(hold, x, "heat.demo.val")
                }
            )

        ## Test against copied objects to see if lapply changed them
        ## all.equal(finalterms.init,finalterms)
        ## all.equal(targs.init,targs)
        ## all.equal(comptable.init,comptable)
        ## rm(finalterms.init, targs.init, comptable.init)
        print("Building ES audience comp...")

        # For each demo in non.model.demos (demos that were not in the model):
        # Apply audienceComp function, placing results in a data.frame...

        # Copy; will test after to lapply to make sure nothing changed.
        ## es.init <- copy(es)
        ## targs.init <- copy(targs)
        ## finalterms.init <- copy(finalterms)

        list.es <-
            lapply(
                non.model.demos,
                function(x) {
                    hold <-
                        callesDatacreation(
                            dsin = es,
                            filters = targs$variable,
                            finalterms = finalterms,
                            extradem = unique(c(extradem, x))
                        )
                    hold[,heat.demo := (x)]
                    if (x %in% names(hold)) {
                        setnames(hold, x, "heat.demo.val")
                    }
                    hold[, heat.demo := as.factor(heat.demo)]
                    return(hold)
                }
            )

        # Test against copied objects to see if lapply changed them
        ## all.equal(es.init, es)
        ## all.equal(targs.init, targs)
        ## all.equal(finalterms.init, finalterms)
        print("Merging survey and ES audience comp...")

        for (i in 1:length(list.es)) { # i <- 1

            audcmp.list[[i]] <-
                esMerge(
                    audcmpSurvey = list.survey[[i]],
                    es.datatab = list.es[[i]],
                    targets = targs
                )

            addcolumn <-
                !(non.model.demos[i] %in% names(audcmp.list[[i]])) &
                non.model.demos[i] %in% extradem

            if (addcolumn) {
                audcmp.list[[i]][, non.model.demos[i]] <-
                    audcmp.list[[i]]$heat.demo.val
            }

        }

    }

    # Handle model.demos
    for (i in model.demos) {
        audcmp.list[[i]] <-
            copy(audcmp)

        audcmp.list[[i]][, heat.demo := i]

        audcmp.list[[i]][, heat.demo.val := get(i)]

    }

    audcmp.stacked <-
        rbindlist(audcmp.list, use.names = TRUE)

    audcmp.stacked[, democombo := NULL]

    ## # Drop non 0/1 heat.demo.val so it places nicely with the heatmaps
    audcmp.stacked <-
        audcmp.stacked[heat.demo.val %in% 0:1, ]

    return(audcmp.stacked)

}


cat("audcomp.weight")

audcomp.weight <-
    function(
        weight.var,
        weight.value,
        threshold = .0001,
        aud.cmp.weight,
        noweight.target,
        sample) {

        ## Log ---------------------------------------------------------------------
        # 2014.04.18 Function Created in S
        # 2014.05.12 Added capacity for multuiple weight variables, and multiple
        #   weight values. Changed it so that you don't provide one weight value,
        #   just one of the values.
        # 2014.05.19 Changed from loop to while conditional dependent on threshold
        # 2015.09.23 Function Created in R. Removed model.names specification.
        #   Added functionality to deselect targets that do not need to be weighted

        # This function takes in a variable to weigh on, a weight ratio, and edits
        #    the corresponding audience composition.

        # weight.var should be a vector of strings indicating the variables to be
        #    weighted on
        # weight.values should be a vector of weight values for the indidence of a
        #    weight variable. IE, the value of the weight of weight.var = 1. The
        #    values should be in the same order as the weight vars they correspond
        #    to and they should eb between 0 and 1.
        # threshold specifies how close the weighted sums need to match the desired
        #    result.  This will sum the absolute differences across all
        #    weight.values and compare to threshold.
        # audcomp names and model selct the audience composition and the models
        #    within that audience compostion umbrella to have the weighting
        #    performed on.
        # targets should be a vector of sstrings indicating the targets that need
        #    to be weighted on

        print.tab <-
            data.frame(
                weight.var = weight.var,
                unweighted = 0,
                weighted = 0,
                desired = weight.value,
                delta = 999)

        noweight <-
            targets$variable[inside("targets", "target_name", noweight.target) == 1]

        dsin.temp <-
            aud.cmp.weight[!(target %in% noweight), ]

        setnames(dsin.temp, sample, "tempsamp")

        dsin.temp[, tempsamp := as.double(tempsamp)]

        nloops <-
            0

        while (sum(print.tab$delta) > threshold) {

            nloops <-
                nloops + 1

            for (j in 1:length(weight.var)) { # j=1

                # get the current ratio for the weight varible, and the weight
                # that should be applied to make it into the desired ratio
                actual <-
                    dsin.temp[
                        ,
                        sum(tempsamp),
                        by = list(target, get(weight.var[j]))]

                setkey(actual, target, get)

                desired <-
                    data.frame(
                        get = as.numeric(0:1),
                        desired = c(1 - weight.value[j], weight.value[j]))

                wt <-
                    merge(actual, desired, by = "get", all = T)

                wt[, wt := na.is.zero(desired, 1/1000000)/V1]

                setnames(wt, "get", weight.var[j])

                # apply the weight
                dsin.temp <-
                    merge(dsin.temp, wt, by = c("target", weight.var[j]))

                dsin.temp[, tempsamp := wt*tempsamp]

                dsin.temp[, wt := NULL]
                dsin.temp[, desired := NULL]
                dsin.temp[, V1 := NULL]

            }

            for (j in 1:length(weight.var)) {
                weighted <-
                    tapply(
                        dsin.temp[, tempsamp],
                        dsin.temp[[weight.var[j]]],
                        sum)/
                    sum(dsin.temp[, tempsamp])

                print.tab$weighted[print.tab$weight.var == weight.var[j]] <-
                    weighted["1"]

            }

            print.tab$delta <-
                abs(print.tab$desired - print.tab$weighted)

        }

        setnames(dsin.temp, "tempsamp", sample)

        out <-
            rbind(
                dsin.temp,
                aud.cmp.weight[target %in% noweight, ]
            )

        return(out)

    }


