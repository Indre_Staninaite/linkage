# Dependencies -----------------------------------------------------------------
require(anesrake)
require(data.table)
require(timeDate)
require(plyr)
require(ggplot2)
# Raking Function --------------------------------------------------------------
create.weight <- function(yes.wt # Weight of a "yes" on the var
                          ){
    # Helper function for rake.it
    return(c(yes.wt, 1 - yes.wt))
}

rake.multi.filter <- function(filters, dat.temp){ # multiple filters at once
        for (fil in filters){
            dat.temp <- # Apply all the filters
                dat.temp[dat.temp[,fil] == 1,]
        }
        return(dat.temp)
    }

rake.it <- function(variables, # Vector of strings
                    dat, # Dataframe with variables
                    wt.vector, # Weights for var == 1, len = len(vars)
                    filters = NULL #Vector of filters to apply
                    ){
    # Return an anesrake object
    # wt.vector <- rake.optim.wt(initial.targets = wt.vector,
    #                            dat = dat,
    #                            variables = variables,
    #                            obj.func = 1)
    # dat$i <- 1:nrow(dat) # Create an indexing column

    dat.temp <- dat[,c(variables,filters,'i')] # Df for manipulation

    dat.temp[,-which(names(dat.temp)=='i')] <- # Numeric to boolean
        apply(dat.temp[,-which(names(dat.temp)=='i')],c(1,2),function(x){x > 0})
    dat.temp <- data.frame(dat.temp) # Apply returns matrix, want df
    dat.temp <- rake.multi.filter(filters, dat.temp)
    dat.temp <- # Remove filter columns
        dat.temp[,setdiff(names(dat.temp),filters)]

    targets <- lapply(wt.vector, create.weight) # Format weights for anesrake

    names(targets) <- variables
    results <- list()
    results$rake <- suppressMessages(
      suppressWarnings(
        anesrake(targets, dat.temp, caseid = dat.temp$i, type = 'nolim')))
    results$dat <- dat
    return(results)
}

rake.optim.prep <- function(initial.targets,
                            dat,
                            variables
                            ){
    initial.prop <- colSums(dat[,variables])/nrow(dat)
    df.temp <- data.frame(init.prop = initial.prop,init.wt = initial.targets)
    return(df.temp)
}

rake.optim.obj <- function(weight.input,
                           sample.prop){
  return(sum((sample.prop-weight.input)^2))
}

rake.optim.obj2 <- function(weight.input,
                           sample.prop){
  return(sum((abs(sample.prop-weight.input)*(1/sample.prop))^2))
}

rake.optim.wt <- function(initial.targets,
                          dat,
                          variables,
                          iters = 200,
                          obj.func = 1
                               ){
    m <- rake.optim.prep(initial.targets, dat,variables)
    setnames(m,c('init.wt','init.prop'),c('t','p'))
    # Variable def:
    # t = target weights
    # p = sample proportion
    # Create bounds matrix for: 0.5*p < t < 2.0*p
    bounds <- matrix(c(m$p * 0.5, m$p * 2.0), nc = 2, byrow = FALSE)
    colnames(bounds) <- c("lower","upper")
    # Convert bounds to ui and ci matrices for constrOptim
    n <- nrow(bounds)
    ui <- rbind(diag(n), -diag(n))
    ci <- c(bounds[,1], - bounds[,2])
    # Add proportions sum to 1 constraint:
    ui <- rbind(ui,rep(1,ncol(ui)))
    ui <- rbind(ui,rep(-1,ncol(ui)))
    ci <- c(ci, 0.9995, -1.0005)
    # Constrained minimization
    # Create starting values
    start.vals = bounds[,'upper'] - bounds[,'lower'] #mid point
    start.vals = start.vals / sum(start.vals) #scale to sum to 1
    if (obj.func == 1){
      my.optm <- constrOptim(theta = start.vals,
                             f = rake.optim.obj,
                             grad = NULL,
                             ui = ui,
                             ci = ci,
                             sample.prop = m$p
                             )
    } else if (obj.func == 2){
      my.optm <- constrOptim(theta = start.vals,
                             f = rake.optim.obj2,
                             grad = NULL,
                             ui = ui,
                             ci = ci,
                             sample.prop = m$p
      )
    }
    return(my.optm$par)

}

rake.process <- function(wt.name, # String, name of new weight variable
                         dat, # Dataframe that was weighted, containing i
                         the.rake,
                         obj.func = 1
                         ){
    # Add weights to data.frame
    my.wt <-
        data.frame(my.wt = the.rake$weightvec,
                   i = the.rake$caseid)
    dat.temp <- # merge weight to dataset on i index (caseid; index of df)
        merge(x = dat, y = my.wt, by = 'i', all.x = TRUE)

    setnames(dat.temp, c("my.wt"),c(paste("obj",obj.func,wt.name, sep = "")))
    return(dat.temp)
}

# Main Function --------------------------------------------------------------
rake.me <- function(variables,
                    dat,
                    wt.vector,
                    output.dir, # Output dir, no trailing slashes
                    filters = NULL,
                    wt.name,
                    obj.func = 1
                    ){
    # Perform whole rake process

    dat$i <- # Add index column to dataframe
        1:nrow(dat)

    my.rake <- # Perform rake process
        rake.it(variables = variables,
                dat = dat,
                wt.vector = wt.vector,
                filters = filters#,
                #obj.func = obj.func
                )
    rake.output <- rake.process(wt.name = wt.name,
                                dat = my.rake$dat,
                                the.rake = my.rake$rake,
                                obj.func = obj.func
                                )

    return(rake.output)
}

rake.writer <- function(the.rake,
                        output.dir, # Output dir w/o trailing slashes
                        file.name # File name including extension
                        ){
    # Write a rake to disk
    write.csv(rake.output, # Write the rake results
              file = file.path(output.dir,file.name))
}

# Weekly weights ---------------------------------------------------------------

dates.to.weeks <- function(date, # Factor vector, from S+ timeDate
                           period.length = 7 # Integer, no. days in period
                           ){
    date <- as.character(date)
    date <- as.timeDate(date)
    date <- as.character(date)
    date <- as.Date(date)
    start.date <- date[1]
    weeks <- as.numeric(date-start.date) %/% period.length
    return(weeks)
}

weekly.weights <- function(dat, # Dataframe
                           date.var = 'week', # name of vector in dat w/ date
                           variables,
                           wt.vector,
                           output.dir, # Output dir, no trailing slashes
                           wt.name, # name of new weight vector
                           filters = NULL
                           ){
    # Split-apply-combine of weights by week.
    names(dat)[which(names(dat)==date.var)] <- c("my.period")
    dat$my.period <- as.factor(dat$my.period)
    my.res <- dlply(dat,.(my.period), function(x){
        rake.me(variables = variables,
                dat = as.data.frame(x) ,
                wt.vector = wt.vector,
                output.dir = output.dir, # Output dir, no trailing slashes
                filters = filters,
                wt.name = wt.name)})
    my.res <-rbindlist(my.res)
    setnames(my.res,c('my.period'),c(date.var))
    return(my.res)
}