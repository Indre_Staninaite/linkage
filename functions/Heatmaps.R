cat("rawHeatmap, ")

rawHeatmap <- function(audcmp, targs, dems, lbd, obj, final.coefs) {

    ## lbd.raw <-
    ##     list()

    ## lbd.tab <-
    ##     list()

    ## heatOut.left <-
    ##     list()

    ## heatOut.right <-
    ##     list()

    ## heatOut <-
    ##     list()

    lbd <-
        data.table(lbd)

    lbd[lift < 0, lift := 0]

    ## Only include targets in targs$variable
    ## (this is a precaution... should always be all)
    z <-
        lbd[target %in% targs$variable, ]

    z[, c("sample", "qre.sample") := NULL]

    ## Automatically get list of keys to merge on
    my.keys <-
        intersect(names(z), names(audcmp))

    ## Merge audcmp and z
    setkeyv(z, my.keys)

    setkeyv(audcmp, my.keys)

    temp <-
        audcmp[z, allow.cartesian = TRUE]

    ## Calculate weighted mean by variable, heat.demo, heat.demo.val, and target
    byvars <-
        c("variable", "heat.demo", "heat.demo.val", "target")

    base <-
        temp[, list(lift = weighted.mean(lift, sample)), by = byvars]

    rm(temp)

    ## cast the data.table wide, a column for each value in variable column,
    ## where the values of the resulting column are from the lift column.
    ## Keep heat.demo, heat.demo.val, and target as 'id'/'by' columns.
    casted <-
        dcast.data.table(
            base,
            heat.demo + heat.demo.val + target ~ variable,
            value.var = 'lift')

    rm(base)

    ## Add names for each heat.demo using the demos tab as a mapping
    demos <-
        as.data.table(demos)

    demos.temp <-
        demos[
            demos$variable %in% casted$heat.demo,
            c('variable','demo_name'),
            with = FALSE]

    setnames(demos.temp, 'variable','heat.demo')

    setkeyv(demos.temp, 'heat.demo')

    setkeyv(casted, 'heat.demo')

    casted <- demos.temp[casted]

    rm(demos.temp)

    ## Add objective and objective_name

    casted[
        ,
        c('objective', 'objective_name') :=
            list(obj$variable, obj$objective_name)]

    # Merge in flrf.target (aka target_name)

    targs <-
        as.data.table(targs)

    targs.temp <-
        targs[, c('variable','target_name'), with = F]

    setnames(
        targs.temp,
        c('variable', 'target_name'),
        c('target', 'flrf.target'))

    setkeyv(targs.temp, 'target')

    setkeyv(casted, 'target')

    rawlifts <-
        targs.temp[casted]

    ## rawlifts <- as.data.frame(rawlifts)

    rm(casted, targs.temp)

    invisible(gc())

    # Create keep column for CS to filter on
    rawlifts[, keep := 1L]

    setnames(rawlifts, 'heat.demo', 'variable')

    rawlifts <-
        as.data.frame(rawlifts)

    # Set keep to 0 for all exelate,fb, and tapestry "not's" (demoval = 0)
    rawlifts[
        intersect(
            grep(
                "seg.|tapestry|FB",
                rawlifts$variable
            ),
            which(rawlifts$demo.val == 0)
        ), "keep"
        ] <-
            0

    # Exclude rows with keep = 0
    rawlifts <-
        rawlifts[rawlifts$keep == 1, ]


    if ("demo_name_not" %nin% names(dems)) {
        dems$demo_name_not <-
            NA
    }

    # Relabeling rows using guide
    manualrows <-
        which(
            rawlifts$heat.demo.val == 0 &
                rawlifts$variable %in%
                dems$variable[
                    !is.na(dems$demo_name_not)
                    ]
        )

    automaticrows <-
        which(
            rawlifts$heat.demo.val == 0 &
                rawlifts$variable %in%
                dems$variable[
                    is.na(dems$demo_name_not)
                    ]
        )

    if (length(manualrows) > 0) {
        rawlifts[manualrows, "demo_name"]  <-
            dems$demo_name_not[
                match(
                    rawlifts$variable[manualrows],
                    dems$variable
                )
                ]
    }

    rawlifts[automaticrows,"demo_name"]  <-
        paste0("Not ",
               dems$demo_name[
                   match(
                       rawlifts$variable[automaticrows],
                       dems$variable
                   )
                   ]
        )

    rawlifts$target <- NULL
    nonsetnames <-
        setdiff(names(rawlifts),
                c("flrf.target", "variable",
                  "demo_name", "heat.demo.val",
                  "objective", "objective_name", "keep"))
    rawlifts <-
        rawlifts[,c("keep", "flrf.target", "variable",
                    "demo_name", "heat.demo.val",
                    "objective", "objective_name", nonsetnames)]

    return(rawlifts)

}


cat("indexHeatmap")

indexHeatmap <- function(htmap, index, targ, lbd, modtype) {

    # Indexes the raw heatmap row, col, or grid wise
    #
    # Args:
    #   htmap: raw heatmap as a data.frame
    #   index: string indicating what type of indexing to do;
    #           options are "row", "column", or "grid

    lbd <-
        data.table(lbd)

    lbd <-
        lbd[sample > 0, ]

    lbd[lift < 0, lift := 0]

    mediacols <-
        names(htmap)[8:length(htmap)]

    targetname <-
        targ$target_name

    htmap[, mediacols] <-
        zero.is.na(htmap[, mediacols])

    if (index == "row") {

        rowmeans <-
            rowMeans(as.data.frame(htmap[, mediacols]), na.rm = TRUE)

        htmap[,mediacols] <-
            htmap[,mediacols]/rowmeans

        return(htmap)
    }

    if (index == "column") {

        for (j in mediacols) { # j <- mediacols[3]

            if (modtype == "Media") {
                medianame <-
                    media$media_name[match(j, media$variable)]
            }

            if (modtype == "Creative") {
                medianame <-
                    creative$creative_name[match(j, creative$variable)]
            }

            colmeans <-
                lbd[lbd$target == targ$variable &
                    lbd$variable == j,
                list(lift = weighted.mean(lift, sample)),
                ]

            if (colmeans > 0) {

                htmap[, j] <-
                    htmap[, j]/as.numeric(colmeans)

            } else {

                htmap[,j] <-
                    rep(0, nrow(htmap))

            }
        }

        htmap <-
            na.is.zero(htmap)

        return(htmap)

    }

    if (index == "grid") {

        mediameans <-
            lbd[lbd$target == targ$variable &
                    lbd$lift > 0,
                list(lift = weighted.mean(lift, sample)),
                by = list(variable)
                ]

        gridmeans <-
            mean(mediameans$lift)

        if (gridmeans == 0 | is.nan(gridmeans)) {
            stop("Can't run Grid index because avg lift to index off of is 0")
        }

        htmap[, mediacols] <-
            htmap[, mediacols]/gridmeans

        htmap <-
            na.is.zero(htmap)

        return(htmap)

    }

}
