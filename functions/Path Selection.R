cat("selectOutputPath, ")

selectOutputPath <- function(override = NULL) {

    # Picks root for export directory based on what exists
    #
    # Args:
    #   override: optional, selects root path for output manually
    #
    # Returns:
    #   override if specified, otherwise the first element of paths that exists

    if (!is.null(override)) {
        return(override)
    }
    else {
        checkpaths <-
            c("S:/1.Projects", "N:/1.Projects", "L:/1.Projects",
              "Z:/Shared/Company", "Z:/Company", "Z:/1.Projects")

        setpaths <-
            c("S:/", "N:/", "L:/",
              "Z:/Shared/Company/", "Z:/Company/", "Z:/")

        paths <-
            setpaths[file.exists(checkpaths)]

        if (length(paths) == 0) {
            paths <-
                "output/"

            dir.create(paths)
        }

        return(paths[1])
    }
}


cat("selectInputPath")

selectInputPath <-
    function(filename, checkpaths = c("input/", "D:/Shared/Data/")) {

        # Picks input folder for files that could live in one of two places
        #
        # Args:
        #   filename: name of file to look for
        #   checkpaths: prioritized lists of folder locations to look in
        # Returns:
        #   path indicating where the filename can be found, with preference
        #   being the earlier elements in checkpaths


        paths <-
            checkpaths[file.exists(paste0(checkpaths, filename))]

        if (length(paths) == 0) {
            stop(
                "File doesn't exist in any checkpaths: \n",
                paste(checkpaths, collapse = ", "))
        }

        out <-
            paste0(paths[1], filename)

        cat(out)

        return(out)
    }
