cat("cleanup, ")

cleanup <- function(zz) {

    # What does this function do?
    #
    # Args:
    #   zz: What is this argument for?
    #
    # Returns:
    #   What does this function return?

    if (nrow(zz) > 1) {

        temp <-
            as.data.frame(
                as.matrix(
                    apply(
                        apply(zz, 2, is.na), 2, as.numeric)
                ) +
                as.matrix(
                    na.is.zero(
                        apply(
                            apply(
                                zz,
                                2,
                                function(x)
                                    gsub('\\s+', '',x)
                            ) == "",
                            2,
                            as.numeric
                        )
                    )
                )
        )

        zz <-
            zz[rowSums(temp) != ncol(temp), colSums(temp) != nrow(temp)]

        if (any(rowSums(temp) == ncol(temp))) {
            warning(paste('blank rows have been removed', sep = ""))
        }

        if (any(colSums(temp) == nrow(temp))) {
            warning(
                paste(
                    'column',
                    names(temp)[colSums(temp) == nrow(temp)],
                    'has been removed ',
                    sep = " "
                )
            )
        }

        return(zz)

    }

    if (nrow(zz) < 2) {

        temp <-
            gsub('\\s+', '',zz) == ""

        temp2 <-
            is.na(zz)

        temp <-
            (matrix(temp) + matrix(temp2)) != 0

        zz1 <-
            zz[, !temp]

        if (any(temp)) {
            warning(
                paste(
                    'column',
                    names(zz)[temp],
                    'has been removed',
                    sep = " "
                )
            )
        }

        return(zz1)

    }
}
